﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;

namespace TaskManager.DbStorage
{
    public class Storage : DbContext, IStorage
    {
        public DbSet<Tasks> Tasks { get; set; }

        public Storage(DbContextOptions<Storage> options)
        : base(options)
        {
            Database.EnsureCreated();
        }

        public void Save()
        {
            SaveChanges();
        }
    }
}
