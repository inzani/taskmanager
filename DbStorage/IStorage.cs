﻿using Microsoft.EntityFrameworkCore;

namespace TaskManager.DbStorage
{
    public interface IStorage
    {
        DbSet<Tasks> Tasks { get; set; }
        void Save();
    }
}
