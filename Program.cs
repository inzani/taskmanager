﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

using TaskManager.DbStorage;
using TaskManager.TaskServices;
using TaskManager.TaskServices.Services;

class Program : DbContext
{
    public static void Main(string[] args)
    {
        var host = CreateHostBuilder(args).Build();
        host.Services.GetRequiredService<ITaskService>().MenuFunction();
    }
    private static readonly ILoggerFactory ConsoleLoggerFactory
            = LoggerFactory.Create(builder => { builder.ClearProviders(); });
    private static IHostBuilder CreateHostBuilder(string[] args)
    {
        return Host.CreateDefaultBuilder(args)
            .ConfigureServices(services =>
            {
                services.AddDbContextPool<Storage, Storage>(options =>
                {
                    options.UseSqlite("Data Source=Task.db");
                    options.UseLoggerFactory(ConsoleLoggerFactory);
                });
                services.AddScoped<IStorage, Storage>();
                services.AddScoped<IAddTaskService, AddTaskService>();
                services.AddScoped<ISearchTask, SearchTask>();
                services.AddScoped<IRemoveTaskService, RemoveTaskService>();
                services.AddTransient<ITaskService, TaskService>();
            });
    }
}