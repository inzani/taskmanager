﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskManager.TaskServices.Services
{
    public interface IRemoveTaskService
    {
        void RemoveTask();
    }
}
