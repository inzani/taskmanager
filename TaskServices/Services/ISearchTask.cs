﻿namespace TaskManager.TaskServices.Services
{
    public interface ISearchTask
    {
        void StartSearch();
    }
}
