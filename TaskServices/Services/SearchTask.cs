﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TaskManager.DbStorage;

namespace TaskManager.TaskServices.Services
{
    public class SearchTask : ISearchTask
    {
        private readonly IStorage _storage;
        private DateTime _datetime;

        public SearchTask(IStorage storage)
        {
            _storage = storage;
            _datetime = DateTime.Now;
        }

        public void StartSearch()
        {
            bool isFinnish = true;

            while (isFinnish)
            {
                var search = _storage.Tasks.Select(p => new { p.Id, p.NameTask, p.status });
                int j = 1;
                int k = 1;
                var TaskSearch = new Dictionary<int, string>() { };
                Console.Clear();
                Console.WriteLine("Текущие задачи");

                foreach (var i in search)
                {
                    TaskSearch.Add(k++, i.NameTask);
                    Console.WriteLine($"{j++} - {i.NameTask} Статус {i.status}");
                }
                Console.WriteLine("Выберите задачу чтобы изменить ее статус\nили введите 0 чтобы выйти в Главное меню");

                var choice = Console.ReadLine();
                int.TryParse(choice, out int ChoiceIsMade);

                if (ChoiceIsMade == 0)
                {
                    isFinnish = false;
                }
                else if(TaskSearch.ContainsKey(ChoiceIsMade) == false)
                {
                    Console.WriteLine("Такой задачи не существует");
                }
                else
                {
                    isFinnish = false;
                    TaskSearch.TryGetValue(ChoiceIsMade, out string TaskName);
                    var TaskStatus = _storage.Tasks.First(p => p.NameTask == TaskName);
                    Console.WriteLine("1 - Чтобы Изменить статус задачи с 'Ожидания' на 'В работе'");
                    Console.WriteLine("2 - Чтобы завершить задание");
                    Console.WriteLine("3 - Чтобы поставить задачу на паузу");
                    Console.WriteLine("4 - Чтобы Продолжить выполнять задачу");
                    var choicestatus = Console.ReadLine();
                    int.TryParse(choicestatus, out int ChoiceIsMadestatus);
                    switch(ChoiceIsMadestatus)
                    {
                        case 1:
                            TaskStatus.status = "В работе";
                            TaskStatus.DatetimeStart = _datetime;
                            _storage.Save();
                            break;
                        case 2:
                            TaskStatus.status = "Выполнение задачи завершено";
                            TaskStatus.DateTimeEnd = _datetime;
                            _storage.Save();
                            break;
                        case 3:
                            TaskStatus.status = "Задача приостановлена";
                            TaskStatus.DateTimeEnd = _datetime;
                            _storage.Save();
                            break;
                        case 4:
                            TaskStatus.status = "В работе";
                            TaskStatus.DatetimePause = TaskStatus.DateTimeEnd.Subtract(TaskStatus.DatetimeStart);
                            TaskStatus.DatetimeStart = _datetime;
                            _storage.Save();
                            break;
                    }
                }
            }
        }
    }
}
