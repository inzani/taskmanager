﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TaskManager.DbStorage;

namespace TaskManager.TaskServices.Services
{
    public class RemoveTaskService :IRemoveTaskService
    {
        private readonly IStorage _storage;

        public RemoveTaskService(IStorage storage)
        {
            _storage = storage;
        }

        public void RemoveTask()
        {
            bool isFinnish = true;
            while (isFinnish)
            {
                var task = _storage.Tasks.ToArray().Select(p => new { p.Id, p.NameTask });
                var TaskSearch = new Dictionary<int, string>() { };
                Console.Clear();
                Console.WriteLine("Текущие задачи");
                int j = 1;
                int k = 1;
                foreach (var i in task)
                {
                    TaskSearch.Add(j++, i.NameTask);
                    Console.WriteLine($"{k++} - {i.NameTask}");
                }

                Console.WriteLine("Выберите задачу которую хотите удалить");
                Console.WriteLine("Чтобы выйти введите 0");

                var choice = Console.ReadLine();
                int.TryParse(choice, out int ChoiceIsMade);

                if (ChoiceIsMade == 0)
                {
                    isFinnish = false;
                }
                else if (TaskSearch.ContainsKey(ChoiceIsMade) == false)
                {
                    Console.WriteLine("Такой задачи не существует");
                }
                else
                {
                    TaskSearch.TryGetValue(ChoiceIsMade, out string TaskName);
                    var Tasks = _storage.Tasks.First(p => p.NameTask == TaskName);
                    _storage.Tasks.Remove(Tasks);
                    _storage.Save();
                    Console.WriteLine($"Задача {TaskName} успешно удалена");
                    Console.ReadKey();
                }
            }
        }
    }
}
