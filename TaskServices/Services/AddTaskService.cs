﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using TaskManager.DbStorage;

namespace TaskManager.TaskServices.Services
{
    public class AddTaskService : IAddTaskService
    {
        private DateTime _datetime;

        private readonly IStorage _storage;

        public AddTaskService(IStorage storage)
        {
            _storage = storage;
            _datetime = DateTime.Now;
        }

        public void AddTask()
        {
            Console.Clear();
            Console.WriteLine("Введите название новой задачи");
            string NameTask = Console.ReadLine();
            Console.Clear();
            Console.WriteLine("Введите цели задачи");
            string GoalTask = Console.ReadLine();
            Console.Clear();
            Console.WriteLine("Желаете ли вы начать выполнение задачи прямо сейчас?\n" +
                "1 - Да\n" +
                "2 - Нет");

            var choice = Console.ReadLine();
            int.TryParse(choice, out int ChoiceIsMade);
            var Task = new Tasks()
            {
                NameTask = NameTask,
                GoalTasks = GoalTask,
                status = "Ожидание"
            };

            _storage.Tasks.Add(Task);
            _storage.Save();

            switch (ChoiceIsMade)
            {
                case 1:
                    var Tasks = _storage.Tasks.First(c => c.NameTask == NameTask);
                    Task.status = "В работе";
                    Tasks.DatetimeStart = _datetime;
                    _storage.Save();
                    break;
                case 2:
                    break;
            }

            Console.WriteLine($"Задача {NameTask} успешно добавлена");
            Console.ReadKey();
        }
    }
}
