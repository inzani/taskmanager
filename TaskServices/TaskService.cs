﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using TaskManager.DbStorage;
using TaskManager.TaskServices.Services;

namespace TaskManager.TaskServices
{
    public class TaskService : ITaskService
    {
        private readonly IAddTaskService _addTaskService;
        private readonly IRemoveTaskService _removeTaskService;
        private readonly ISearchTask _searchtask;
        private readonly IStorage _storage;

        public TaskService( IAddTaskService taskService, IRemoveTaskService removeTaskService, ISearchTask searchTask,IStorage storage)
        {
            _addTaskService = taskService;
            _removeTaskService = removeTaskService;
            _searchtask = searchTask;
            _storage = storage;
        }
        private void EndTask()
        {
            var search = _storage.Tasks.Select(p => new { p.Id, p.NameTask, p.status, p.DatetimePause,p.DateTimeEnd,p.DatetimeStart });
            Console.Clear();
            Console.WriteLine("Завершенные задачи");
            foreach (var i in search)
            {
                if(i.status == "Выполнение задачи завершено")
                {
                    var datetime = i.DateTimeEnd.Subtract(i.DatetimeStart);
                    var DateTime = datetime.Add(i.DatetimePause);
                    Console.WriteLine($"{i.NameTask} Статус {i.status} Всего затрачено времени: {DateTime}");
                }
            }
            Console.ReadKey();
        }

        public void MenuFunction()
        {
            while (true) {
                Console.WriteLine("Главное меню\n" +
                "1-Открыть список задач\n" +
                "2-Добавить задачу\n" +
                "3-Удалить задачу\n" +
                "4-Посмотреть Завершенные задачи");
                var choice = Console.ReadLine();
                int.TryParse(choice, out int selectchoice);
                switch (selectchoice)
                {
                    case 1:
                        _searchtask.StartSearch();
                        break;
                    case 2:
                        _addTaskService.AddTask();
                        break;
                    case 3:
                        _removeTaskService.RemoveTask();
                        break;
                    case 4:
                        EndTask();
                        break;
                }
            }
        }
    }
}
